#include <iostream>
#include <vector>
#include <chrono>
#include <zconf.h>
#include <ncurses.h>
#include "Squirrel.h"
#include "Sloth.h"
#include "Tree.h"
#include <cstring>

#define test_errno(msg) do{if (errno) {perror(msg); exit(EXIT_FAILURE);}} while(0)

using namespace std;
const int SquirrelsNumber = 6;
const int SlothNumber = 3;
const int TreeNumber = 2;
const int HollowNumber = SquirrelsNumber;
int maxConesIceCave = 50;

struct IceCave {
    int state; // 1 - zajęte 0 - wolne 2 - napełnianie
    int numberOfCones;
};

void initLocks();

IceCave iceCave = {0, maxConesIceCave};

pthread_mutex_t iceCaveStatusLock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t iceCaveStatusCond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t hollowStateLock[HollowNumber];
pthread_cond_t hollowStateCond[HollowNumber];

vector<Squirrel *> squirrel;
vector<Sloth *> sloth;
vector<Tree *> tree;
vector<Hollow *> hollow;


void *squirrelsLife(void *_argSquirrel) {
    Squirrel *squirrel = (Squirrel *) _argSquirrel;
    while (true) {
        pthread_mutex_lock(&iceCaveStatusLock);
        while (iceCave.state != 0 || iceCave.numberOfCones == 0) {
            squirrel->DoWait();
            pthread_cond_wait(&iceCaveStatusCond, &iceCaveStatusLock);
        }  //cave is empty and has cones
        iceCave.state = 1;
        int cone = rand() % 4 + 1;
        if (cone > iceCave.numberOfCones) {
            cone = iceCave.numberOfCones;
        }
        squirrel->DoCollect(cone);
        iceCave.numberOfCones -= cone;
        iceCave.state = 0;
        pthread_cond_signal(&iceCaveStatusCond);
        pthread_mutex_unlock(&iceCaveStatusLock);

        squirrel->DoGo();
        squirrel->DoWait();
        pthread_mutex_lock(&hollowStateLock[squirrel->id]);
        while (hollow.at(squirrel->id)->state != Hollow::Empty) {

            pthread_cond_wait(&hollowStateCond[squirrel->id], &hollowStateLock[squirrel->id]);
        }
        hollow.at(squirrel->id)->DoIsSquirrel();
        squirrel->DoRest();
            squirrel->DoEat();
            squirrel->DoRest();
        hollow.at(squirrel->id)->DoEmpty();
        pthread_cond_signal(&hollowStateCond[squirrel->id]);
        pthread_mutex_unlock(&hollowStateLock[squirrel->id]);
        squirrel->DoGo();
    }
    return NULL;
}

void *slothsLife(void *_argSloth) {
    Sloth *sloth = (Sloth *) _argSloth;
    while (true) {
        int idHollow = rand() % SquirrelsNumber + 0;
        sloth->DoWait(idHollow);
        pthread_mutex_lock(&hollowStateLock[idHollow]);
        if (hollow.at(idHollow)->state != Hollow::Empty) {

            pthread_cond_wait(&hollowStateCond[idHollow], &hollowStateLock[idHollow]);
        }
        hollow.at(idHollow)->DoIsSloth();
        int cone = rand() % 2 + 1;
        if (cone > hollow.at(idHollow)->coneCount) {
            cone = hollow.at(idHollow)->coneCount;
        }
        sloth->DoSteal(idHollow, cone);
        hollow.at(idHollow)->addCones(-cone);
        hollow.at(idHollow)->DoEmpty();
        pthread_cond_signal(&hollowStateCond[idHollow]);
        pthread_mutex_unlock(&hollowStateLock[idHollow]);
        sloth->DoRest();
    }
    return NULL;
}

void *treesLife(void *_argTree) {
    Tree *tree = (Tree *) _argTree;
    while (true) {
        errno = pthread_mutex_lock(&iceCaveStatusLock);
        while (iceCave.numberOfCones != 0 && iceCave.state != 2) {
            pthread_cond_wait(&iceCaveStatusCond, &iceCaveStatusLock);
        }
        iceCave.state = 2;
        tree->DoThrowCones();

        usleep(200000);
        iceCave.numberOfCones++;
        if (iceCave.numberOfCones >= maxConesIceCave) {
            iceCave.state = 0;
        }
        pthread_cond_signal(&iceCaveStatusCond);
        errno = pthread_mutex_unlock(&iceCaveStatusLock);
        tree->DoRest();
    }
    return NULL;
}

void *guiLife(void *_argGui) {
    initscr(); // rozpoczęcie trybu curses
    start_color();
    mvprintw(37,63,"Squirrels and Sloths");
    refresh();
    init_color(COLOR_BLUE, 220, 520, 700);
    init_color(COLOR_RED, 700, 200, 200);
    init_pair(1, COLOR_BLUE, COLOR_BLACK);
    init_pair(2, COLOR_YELLOW, COLOR_BLACK);
    init_pair(3, COLOR_RED, COLOR_BLACK);
    init_pair(4, COLOR_GREEN, COLOR_BLACK);
    init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
    noecho();
    nodelay(stdscr, TRUE);
    curs_set(0);

    WINDOW *squirrels_world[5];
    WINDOW *sloth_world[3];
    WINDOW *cave_world;
    WINDOW *tree_world[2];
    WINDOW *squirrelImage;
    squirrelImage = newwin(10, 18, 27, 65);

    wattron(squirrelImage, COLOR_PAIR(2));
    mvwprintw(squirrelImage, 0, 0, "              _");
    mvwprintw(squirrelImage, 1, 0, "          .-'` `}");
    mvwprintw(squirrelImage, 2, 0, "  _./)   /       }");
    mvwprintw(squirrelImage, 3, 0, ".'o   \\ |       }");
    mvwprintw(squirrelImage, 4, 0, "'.___.'`.\\    {`");
    mvwprintw(squirrelImage, 5, 0, "/`\\_/  , `.    }");
    mvwprintw(squirrelImage, 6, 0, "\\=' .-'   _`\\  {");
    mvwprintw(squirrelImage, 7, 0, "`'`;/      `,  }");
    mvwprintw(squirrelImage, 8, 0, "   _\\       ;  }");
    mvwprintw(squirrelImage, 9, 0, "  /__`;-...'--'");
    wrefresh(squirrelImage);

    for (int i = 0; i < squirrel.size(); i++) {

        squirrels_world[i] = newwin(6, 31, 6 * i + 2, 1);
        wattron(squirrels_world[i], COLOR_PAIR(1));
        box(squirrels_world[i], 0, 0);
        wrefresh(squirrels_world[i]);
    }

    for (int i = 0; i < sloth.size(); i++) {
        sloth_world[i] = newwin(6, 25, 6 * i + 2, 35);
        wattron(sloth_world[i], COLOR_PAIR(3));
        box(sloth_world[i], 0, 0);
        wrefresh(sloth_world[i]);
    }
    cave_world = newwin(5, 21, 2, 62);
    wattron(cave_world, COLOR_PAIR(5));
    box(cave_world, 0, 0);
    wrefresh(cave_world);

    for (int i = 0; i < tree.size(); i++) {
        tree_world[i] = newwin(4, 21, 4 * i + 8, 62);
        wattron(tree_world[i], COLOR_PAIR(4));
        box(tree_world[i], 0, 0);
        wrefresh(tree_world[i]);
    }

    do {
        for (int squirrelId = 0; squirrelId < squirrel.size(); squirrelId++) {

            mvwprintw(squirrels_world[squirrelId], 0, 1, "  Squirrel: %1d ",
                      squirrel.at(squirrelId)->id);
            mvwprintw(squirrels_world[squirrelId], 1, 1, "State: ");
            mvwprintw(squirrels_world[squirrelId], 1, 8, "%-10s",
                      (char *) squirrel.at(squirrelId)->getStateName().c_str());
            mvwprintw(squirrels_world[squirrelId], 2, 1, "Cones in a paw: %-3d ",
                      squirrel.at(squirrelId)->conesInAPaw);
            wrefresh(squirrels_world[squirrelId]);
        }
        for (int hollowId = 0; hollowId < hollow.size(); hollowId++) {
            mvwprintw(squirrels_world[hollowId], 3, 1, "State of hollow: %-10s",
                      hollow.at(hollowId)->getStateName().c_str());
            mvwprintw(squirrels_world[hollowId], 4, 1, "Cones in hollow: %-3d",
                      hollow.at(hollowId)->coneCount);
            wrefresh(squirrels_world[hollowId]);
        }

        for (int slothId = 0; slothId < sloth.size(); slothId++) {
            mvwprintw(sloth_world[slothId], 0, 1, "Sloth: %-3d",
                      sloth.at(slothId)->id);
            mvwprintw(sloth_world[slothId], 1, 1, "State: %-10s",
                      (char *) sloth.at(slothId)->getStateName().c_str());
            if (sloth.at(slothId)->robbedHollow == 666) {
                mvwprintw(sloth_world[slothId], 2, 1, "%-20s", "");
            } else {
                mvwprintw(sloth_world[slothId], 2, 1, "Robs the squirrel: %d",
                          sloth.at(slothId)->robbedHollow);
            }
            mvwprintw(sloth_world[slothId], 3, 1, "Robbed cones: %-3d",
                      sloth.at(slothId)->robbedCones);
            wrefresh(sloth_world[slothId]);
        }
        mvwprintw(cave_world, 0, 1, "Ice Cave");
        mvwprintw(cave_world, 1, 1, "State: %-3d", iceCave.state);
        mvwprintw(cave_world, 2, 1, "Cones count: %-3d", iceCave.numberOfCones);
        wrefresh(cave_world);

        for (int treeId = 0; treeId < tree.size(); treeId++) {
            mvwprintw(tree_world[treeId], 0, 1, "Tree: %-3d",
                      tree.at(treeId)->id);
            mvwprintw(tree_world[treeId], 1, 1, "State: %-10s",
                      (char *) tree.at(treeId)->getStateName().c_str());
            wrefresh(tree_world[treeId]);
        }

    } while (1);


    for (int i = 0; i < sloth.size(); i++)
        delwin(sloth_world[i]);
    for (int i = 0; i < squirrel.size(); i++)
        delwin(squirrels_world[i]);
    //getchar(); // czeka na użytkownika
    return NULL;
}

int main() {
    initLocks();
    srand(time(NULL));
    pthread_t squirrel_thread[SquirrelsNumber];
    pthread_t sloth_thread[SlothNumber];
    pthread_t tree_thread[TreeNumber];
    pthread_t gui_thread;

    // Tworzenie wątku odpowiedzialnego za wygląd
    int statusCode = pthread_create(&gui_thread, NULL, guiLife, NULL);
    if (statusCode != 0) {
        fprintf(stderr, ("pthread_create() failed with error #%d: '%s'\n", statusCode, strerror(statusCode)));
        getch();
        exit(EXIT_FAILURE);
    }

    // utworzenie dziupli
    for (int i = 0; i < HollowNumber; i++) {
        Hollow *temp = new Hollow(i);
        temp->id = i;
        hollow.push_back(temp);
    }

    // Utworzenie watku wiewiórki
    for (int i = 0; i < SquirrelsNumber; i++) {
        squirrel.push_back(new Squirrel(i, hollow.at(i)));
        int statusCode = pthread_create(&squirrel_thread[i], NULL, squirrelsLife, (void *) squirrel.at(i));
        if (statusCode != 0) {
            printw("pthread_create() failed with error #%d: '%s'\n", statusCode, strerror(statusCode));
            getch();
            exit(EXIT_FAILURE);
        }
    }

    // Utworzenie wątku leniwca
    for (int i = 0; i < SlothNumber; i++) {
        Sloth *temp = new Sloth(i);
        sloth.push_back(temp);
        int statusCode = pthread_create(&sloth_thread[i], NULL, slothsLife, (void *) sloth.at(i));
        if (statusCode != 0) {
            fprintf(stderr, ("pthread_create() failed with error #%d: '%s'\n", statusCode, strerror(statusCode)));
            getch();
            exit(EXIT_FAILURE);
        }
    }

    // Utworzenie watku drzewa
    for (int i = 0; i < TreeNumber; i++) {
        Tree *temp = new Tree(i, 0);
        tree.push_back(temp);
        int statusCode = pthread_create(&tree_thread[i], NULL, treesLife, (void *) tree.at(i));
        if (statusCode != 0) {
            fprintf(stderr, ("pthread_create() failed with error #%d: '%s'\n", statusCode, strerror(statusCode)));
            getch();
            exit(EXIT_FAILURE);
        }
    }

    // Oczekiwania na zakończenia wątków wiewiórki
    for (int i = 0; i < SquirrelsNumber; i++) {
        errno = pthread_join(squirrel_thread[i], NULL);
        test_errno("pthread-join");
    }
    // Oczekiwania na zakończenia wątków leniwca
    for (int i = 0; i < SlothNumber; i++) {
        errno = pthread_join(sloth_thread[i], NULL);
        test_errno("pthread-join");
    }
    // Oczekiwania na zakończenia wątków drzewa
    for (int i = 0; i < TreeNumber; i++) {
        errno = pthread_join(tree_thread[i], NULL);
        test_errno("pthread-join");
    }

    // Oczekiwania na zakończenia wątu GUI
        errno = pthread_join(gui_thread, NULL);
        test_errno("pthread-join");

    getchar();
    endwin(); // zakończenie trybu curses

    for (int i = 0; i < SquirrelsNumber; i++) {
        delete squirrel.at(i);
    }
    for (int i = 0; i < SlothNumber; i++) {
        delete squirrel.at(i);
    }

    for (int i = 0; i < TreeNumber; i++) {
        delete squirrel.at(i);
    }

    return 0;
}

void initLocks() {
    for (int i = 0; i < HollowNumber; i++) {
        hollowStateLock[i] = PTHREAD_MUTEX_INITIALIZER;
        hollowStateCond[i] = PTHREAD_COND_INITIALIZER;
    }
}

