#include "Sloth.h"

Sloth::Sloth(int id){
    this->id = id;
    this->state = Rest;
    this->robbedHollow = 666;
    this->robbedCones = 0;
}

string Sloth::getStateName() {
    return slothStateNames[state];
}

void Sloth::DoRest() {
    this->state = Rest;
    this->robbedHollow = 666;
     usleep(600000);
}

void Sloth::DoWait(int idHollow) {
    this->robbedHollow = idHollow;
    this->state = Wait;
}

void Sloth::DoSteal(int idHollow, int cones) {
    this->state = Steal;
    this->robbedCones += cones;
    this->robbedHollow = idHollow;
    usleep(400000);
}
