#include "Hollow.h"

Hollow::Hollow(int id) {
    this->id = id;
    this->coneCount = 10;
    this->state = Empty;
}

void Hollow::DoIsSquirrel() {
    this->state = isSquirrel;
}

void Hollow::DoIsSloth() {
    this->state = isSloth;
}

void Hollow::addCones(int cones){
    this->coneCount += cones;
}

void Hollow::DoEmpty() {
    this->state = Empty;
}

string Hollow::getStateName() {
    return hollowStateNames[state];
}

