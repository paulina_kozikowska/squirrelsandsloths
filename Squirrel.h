#ifndef SQUIRRELSANDSLOTHS_SQUIRREL_H
#define SQUIRRELSANDSLOTHS_SQUIRREL_H
#include <iostream>
#include "Hollow.h"
#include <string>
using namespace std;

class Squirrel {
private:
    enum SquirrelState {
        Rest,
        Eat,
        Go,
        Wait,
        Collect
    };
    string squirrelsStateNames[5] = {"Rest", "Eat", "Go", "Wait", "Collect"};

public:
    Squirrel(int id, Hollow *hollow);
    int id;
    SquirrelState state;
    int conesInAPaw = 0;

    string getStateName();
    void DoRest();
    void DoEat();
    void DoGo();
    void DoWait();
    void DoCollect(int quantityCone);


    Hollow *hollow;
};


#endif //SQUIRRELSANDSLOTHS_SQUIRREL_H
