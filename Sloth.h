#ifndef SQUIRRELSANDSLOTHS_SLOTH_H
#define SQUIRRELSANDSLOTHS_SLOTH_H
#include <iostream>
#include <zconf.h>
#include <string>

using namespace std;

class Sloth {
private:
    enum SlothState {
        Rest,
        Wait,
        Steal
    };
    string slothStateNames[5] = {"Rest", "Wait", "Steal"};
public:
    Sloth(int id);
    int id;
    int state;
    int robbedHollow;
    int robbedCones;
    string getStateName();
    void DoRest();
    void DoWait(int idHollow);
    void DoSteal(int idHollow, int cones);
};


#endif //SQUIRRELSANDSLOTHS_SLOTH_H
