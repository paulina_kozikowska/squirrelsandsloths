#ifndef SQUIRRELSANDSLOTHS_TREE_H
#define SQUIRRELSANDSLOTHS_TREE_H
#include <iostream>
#include <zconf.h>
#include <string>
using namespace std;

class Tree {
private:
    enum TreeState {
        Rest,
        ThrowCones
    };
    string treeStateNames[5] = {"Rest", "ThrowCones"};
public:
    Tree (int id, int state);
    int id;
    int state;
    void DoRest();
    void DoThrowCones();
    string getStateName();

};


#endif //SQUIRRELSANDSLOTHS_TREE_H
