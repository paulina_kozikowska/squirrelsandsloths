#include <zconf.h>
#include <string>
#include "Squirrel.h"

Squirrel::Squirrel(int id, Hollow* hollow)  {
    this->id = id;
    this->state = Go;
    this->conesInAPaw = 0;
    this->hollow = hollow;
}

        string Squirrel::getStateName() {
        return squirrelsStateNames[state];
    }

void Squirrel::DoRest() {
    this->state = Rest;
    this->hollow->coneCount += conesInAPaw;
    conesInAPaw = 0;
    usleep(400000);
}

void Squirrel::DoEat() {
    srand( time( NULL ) );
    this->state = Eat;
    this->hollow->DoIsSquirrel();
    int cone = rand()%2 + 1;
    if (cone> hollow->coneCount){
        cone = hollow->coneCount;
    }
    this->hollow->coneCount-=cone;
    usleep(300000); // 300 milisekund
}

void Squirrel::DoGo() {
    this->state = Go;
    usleep(600000); // 600 milisekund
}

void Squirrel::DoWait() {
    this->state = Wait;
}

void Squirrel::DoCollect(int quantityCone) {
    this->state = Collect;
    conesInAPaw = quantityCone;
    usleep(500000); // 500 milisekund
}