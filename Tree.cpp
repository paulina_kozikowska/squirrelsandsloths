#include "Tree.h"

Tree::Tree(int id, int state) {
    this->id = id;
    this->state = state;
}

void Tree::DoRest() {
    this->state = Rest;
    usleep(100000);
}

void Tree::DoThrowCones() {
    this->state = ThrowCones;
}

string Tree::getStateName() {
    return treeStateNames[state];
}
