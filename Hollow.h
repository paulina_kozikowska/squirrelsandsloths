#ifndef SQUIRRELSANDSLOTHS_HOLLOW_H
#define SQUIRRELSANDSLOTHS_HOLLOW_H
#include <iostream>
#include <string>
using namespace std;

class Hollow {
private:
public:
    enum HollowState {
        Empty,
        isSquirrel,
        isSloth
    };
    Hollow(int id);
    int id;
    int coneCount;
    int state;
    string getStateName();
    void DoEmpty();
    void DoIsSquirrel();
    void DoIsSloth();
    void addCones(int cones);
    string hollowStateNames[5] = {"Empty", "isSquirrel", "isSloth"};

};



#endif //SQUIRRELSANDSLOTHS_HOLLOW_H
